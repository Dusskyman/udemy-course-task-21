import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  DateTime initDateTime = DateTime.now();
  bool switchValue = true;
  double sliderValue = 0.0;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool overPowered() {
    if (widget.sliderValue >= 1) {
      setState(() {
        widget.switchValue = false;
      });
      return false;
    }
    setState(() {
      widget.switchValue = true;
    });
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: !widget.switchValue ? Colors.black54 : Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: double.infinity,
            height: 100,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.time,
              initialDateTime: widget.initDateTime,
              onDateTimeChanged: (value) => widget.initDateTime = value,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              !widget.switchValue
                  ? Transform.rotate(
                      angle: 6,
                      child: Text(
                        'Turn the light on',
                        style: GoogleFonts.indieFlower(
                          fontSize: 40,
                          color: Color.fromRGBO(57, 255, 20, 1),
                        ),
                      ),
                    )
                  : Text(
                      'Turn the light off',
                      style: TextStyle(
                        fontSize: 30,
                      ),
                    ),
              CupertinoSwitch(
                activeColor: Colors.black,
                trackColor: Color.fromRGBO(57, 255, 20, 1),
                value: widget.switchValue,
                onChanged: (value) {
                  setState(() {
                    widget.switchValue = value;
                    if (widget.sliderValue == 1) {
                      widget.sliderValue = 0;
                    }
                  });
                },
              ),
            ],
          ),
          CupertinoSlider(
            activeColor: !widget.switchValue
                ? Color.fromRGBO(224, 231, 34, 1)
                : Colors.grey,
            value: widget.sliderValue,
            onChangeEnd: (value) {
              setState(() {});
            },
            onChanged: (value) {
              setState(() {
                widget.sliderValue = value;
              });
            },
          ),
          !overPowered()
              ? Transform.rotate(
                  angle: -6,
                  child: Text(
                    'Overpowered',
                    style: GoogleFonts.architectsDaughter(
                      fontSize: 50,
                      color: Color.fromRGBO(224, 231, 34, 1),
                    ),
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }
}
